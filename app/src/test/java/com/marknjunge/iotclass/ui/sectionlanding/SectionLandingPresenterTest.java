package com.marknjunge.iotclass.ui.sectionlanding;

import com.marknjunge.iotclass.MockDatabase;
import com.marknjunge.iotclass.models.Sensor;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class SectionLandingPresenterTest {
    private MockActivity mockActivity;

    @Before
    public void setup() {
        mockActivity = new MockActivity();

        MockDatabase databaseRepository = new MockDatabase();
        SectionLandingPresenter presenter = new SectionLandingPresenter(mockActivity, databaseRepository, "section");
        presenter.getUserSensors();
    }

    @Test
    public void shouldDisplayMessage(){
        Assert.assertEquals(true, mockActivity.messageDisplayed);
    }

    @Test
    public void shouldDisplaySensors(){
        Assert.assertEquals(true, mockActivity.sensorsDisplayed);
    }

    @Test
    public void shouldDisplayNoSensorsMessage(){
        Assert.assertEquals(true, mockActivity.noSensorsTextDisplayed);
    }

    private class MockActivity implements SectionLandingView {

        private boolean messageDisplayed;
        private boolean sensorsDisplayed;
        private boolean noSensorsTextDisplayed;

        @Override
        public void displayMessage(String message) {
            messageDisplayed = true;
        }

        @Override
        public void displaySensors(List<Sensor> sensors) {
            sensorsDisplayed = true;
        }

        @Override
        public void displayNoSensorsText() {
            noSensorsTextDisplayed = true;
        }
    }
}