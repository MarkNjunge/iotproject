package com.marknjunge.iotclass.dagger;

import com.marknjunge.iotclass.ui.main.MainActivity;
import com.marknjunge.iotclass.ui.moredetails.MoreDetailsActivity;
import com.marknjunge.iotclass.ui.sectionlanding.SectionLandingActivity;
import com.marknjunge.iotclass.ui.signup.SignUpActivity;

import dagger.Component;

@Component(modules = {DatabaseModule.class})
public interface AppComponent {
    void inject(MainActivity mainActivity);
    void inject(SectionLandingActivity sectionLandingActivity);
    void inject(MoreDetailsActivity moreDetailsActivity);
    void inject(SignUpActivity signUpActivity);
}
