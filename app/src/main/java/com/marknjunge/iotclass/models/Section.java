package com.marknjunge.iotclass.models;

import java.util.List;

public class Section {
    private String sectionName;
    private List<String> sensors;

    public Section(String sectionName, List<String> sensors) {
        this.sectionName = sectionName;
        this.sensors = sensors;
    }

    public String getSectionName() {
        return sectionName;
    }

    public List<String> getSensors() {
        return sensors;
    }

    @Override
    public String toString() {
        return "Section{" +
                "sectionName='" + sectionName + '\'' +
                ", sensors=" + sensors +
                '}';
    }
}
