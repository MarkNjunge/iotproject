package com.marknjunge.iotclass;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.marknjunge.iotclass.models.DataReading;
import com.marknjunge.iotclass.models.Section;
import com.marknjunge.iotclass.models.Sensor;
import com.marknjunge.iotclass.network.DatabaseListener;
import com.marknjunge.iotclass.network.DatabaseRepository;

import java.util.Arrays;
import java.util.Collections;


public class MockDatabase implements DatabaseRepository {

    @Override
    public void getLastValue(String section, String sensor, DatabaseListener.SingleValueListener listener) {
        listener.valuesReceived("10", 123456L);

        listener.taskFailed("Some message");
    }

    @Override
    public void getValues(String section, String sensor, int count, DatabaseListener.DBValuesListener valuesListener) {
        valuesListener.valuesReceived(Arrays.asList(
                new DataReading("10", 123456L),
                new DataReading("10", 123456L),
                new DataReading("10", 123456L))
        );

        valuesListener.taskFailed("Some message");
    }

    @Override
    public void getAnalysedData(String section, String sensor, DatabaseListener.AnalysedDataListener dataListener) {
        dataListener.valuesReceived("min","max", "avg");

        dataListener.taskFailed("Some message");
    }

    @Override
    public void getUserSections(DatabaseListener.UserSectionsListener sectionsListener) {
        sectionsListener.valueReceived(Collections.<Section>emptyList());

        sectionsListener.valueReceived(Arrays.asList(
                new Section("Section 1", null),
                new Section("Section 2", null),
                new Section("Section 3", null))
        );

        sectionsListener.taskFailed("Some message");
    }

    @Override
    public void getUserSensors(String section, DatabaseListener.SensorsListener sensorsListener) {
        sensorsListener.valuesReceived(Arrays.asList(
                new Sensor("id", "name", "lastReading", 123456L),
                new Sensor("id", "name", "lastReading", 123456L),
                new Sensor("id", "name", "lastReading", 123456L)
        ));

        sensorsListener.valuesReceived(Collections.<Sensor>emptyList());

        sensorsListener.taskFailed("Some message");
    }

    @Override
    public void addUserDetails(FirebaseUser user, String name, String email, DatabaseListener.DBTask dbTask) {
        dbTask.taskCompleted();

        dbTask.taskFailed("Some message");
    }

    @Override
    public void attachListener(DatabaseReference databaseReference, ValueEventListener valueEventListener) {

    }

    @Override
    public void detachListener(DatabaseReference databaseReference, ValueEventListener valueEventListener) {

    }
}
