package com.marknjunge.iotclass.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.marknjunge.iotclass.R;

import java.util.List;

public class MyLineChart implements OnChartValueSelectedListener {
    private final LineChart mLineChart;

    private final int accentColor, textColor, gridColor;

    private final Context mContext;

    public MyLineChart(Context context, View lineChart) {
        mContext = context;
        mLineChart = (LineChart) lineChart;

        accentColor = ContextCompat.getColor(context, R.color.colorAccent);
        textColor = ContextCompat.getColor(context, R.color.colorChartText);
        gridColor = ContextCompat.getColor(context, R.color.colorChartGrid);
    }

    public void initializeChart() {
        mLineChart.setHighlightPerDragEnabled(false);
        mLineChart.setScaleYEnabled(false);
        mLineChart.setDescription(null);

        mLineChart.getXAxis().setDrawGridLines(false);
        mLineChart.getAxisRight().setEnabled(false);

        mLineChart.getLegend().setEnabled(false);

        mLineChart.getAxisLeft().setDrawGridLines(false);
        mLineChart.getAxisLeft().setTextColor(textColor);
        mLineChart.getXAxis().setTextColor(textColor);
        mLineChart.getXAxis().setDrawLabels(false);

        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        mLineChart.setOnChartValueSelectedListener(this);
    }

    public void setChartValues(List<Entry> entries) {
        LineDataSet dataSet = new LineDataSet(entries, "Values");
        dataSet.setColor(accentColor);
        dataSet.setHighLightColor(gridColor);
        dataSet.setValueTextColor(textColor);
        dataSet.setDrawCircles(false);
        dataSet.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
        Drawable drawable = ContextCompat.getDrawable(mContext, R.drawable.fade_red);
        dataSet.setFillDrawable(drawable);
        dataSet.setDrawFilled(true);

        float max = 0f;
        for (Entry e : entries) {
            if (e.getY() > max)
                max = e.getY();
        }

        mLineChart.getAxisLeft().setAxisMaximum(max + 1);

        LineData lineData = new LineData(dataSet);
        lineData.setDrawValues(false);
        mLineChart.setData(lineData);
        mLineChart.invalidate();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
