package com.marknjunge.iotclass.ui.moredetails;

import com.github.mikephil.charting.data.Entry;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.marknjunge.iotclass.models.DataReading;
import com.marknjunge.iotclass.models.Sensor;
import com.marknjunge.iotclass.network.DatabaseListener;
import com.marknjunge.iotclass.network.DatabaseRepository;
import com.marknjunge.iotclass.utils.RandomUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class MoreDetailsPresenter {

    private int DB_VALUES_COUNT = 30;
    private final MoreDetailsView activityView;
    private DatabaseRepository databaseRepository;
    private final Sensor sensor;
    private String section;
    private Map<DatabaseReference, ValueEventListener> eventListenerMap;

    MoreDetailsPresenter(MoreDetailsView activityView, DatabaseRepository databaseRepository, Sensor sensor, String section) {
        this.activityView = activityView;
        this.databaseRepository = databaseRepository;
        this.sensor = sensor;
        this.section = section;
        eventListenerMap = new HashMap<>();
    }

    void getData() {
        getLastReading();
        getChartEntries();
        getAnalysedData();
    }

    private void getLastReading() {
        DatabaseListener.SingleValueListener listener = new DatabaseListener.SingleValueListener() {
            @Override
            public void valuesReceived(String value, long timestamp) {
                activityView.updateLastValue(value, RandomUtils.getReadableTime(timestamp));
            }

            @Override
            public void taskFailed(String message) {
                activityView.displayMessage(message);
            }

            @Override
            public void listenerReceived(DatabaseReference databaseReference, ValueEventListener eventListener) {
                eventListenerMap.put(databaseReference, eventListener);
            }
        };

        databaseRepository.getLastValue(section, sensor.getSensorID(), listener);
    }

    private void getChartEntries() {
        DatabaseListener.DBValuesListener valuesListener = new DatabaseListener.DBValuesListener() {
            @Override
            public void valuesReceived(List<DataReading> values) {
                if (values.size() > 0) {
                    List<Entry> entries = new ArrayList<>();
                    for (DataReading dataReading : values) {
                        //TODO check for not a number
                        entries.add(new Entry(values.indexOf(dataReading), Float.valueOf(dataReading.getValue())));
                    }
                    activityView.displayChartReadings(entries);
                    DataReading lastValue = values.get(values.size() - 1);
                    activityView.updateLastValue(lastValue.getValue(), RandomUtils.getReadableTime(lastValue.getTimestamp()));
                }
            }

            @Override
            public void taskFailed(String message) {
                activityView.displayMessage(message);
            }

            @Override
            public void listenerReceived(DatabaseReference databaseReference, ValueEventListener eventListener) {
                eventListenerMap.put(databaseReference, eventListener);
            }
        };

        databaseRepository.getValues(section, sensor.getSensorID(), DB_VALUES_COUNT, valuesListener);
    }

    private void getAnalysedData() {
        databaseRepository.getAnalysedData(section, sensor.getSensorID(), new DatabaseListener.AnalysedDataListener() {
            @Override
            public void valuesReceived(String min, String max, String avg) {
                activityView.displayAnalysedData(min, max, avg);
            }

            @Override
            public void taskFailed(String message) {
                activityView.displayMessage(message);
            }

            @Override
            public void listenerReceived(DatabaseReference databaseReference, ValueEventListener eventListener) {
                eventListenerMap.put(databaseReference, eventListener);
            }
        });
    }

    void activityPaused() {
        for (Map.Entry<DatabaseReference, ValueEventListener> entry : eventListenerMap.entrySet()) {
            databaseRepository.detachListener(entry.getKey(), entry.getValue());
        }
    }
}
