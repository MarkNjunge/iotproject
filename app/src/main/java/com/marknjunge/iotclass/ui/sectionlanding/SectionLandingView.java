package com.marknjunge.iotclass.ui.sectionlanding;

import com.marknjunge.iotclass.models.Sensor;

import java.util.List;

interface SectionLandingView {
    void displayMessage(String message);
    void displaySensors(List<Sensor> sensors);
    void displayNoSensorsText();
}
