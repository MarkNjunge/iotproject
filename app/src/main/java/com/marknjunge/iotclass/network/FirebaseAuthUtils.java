package com.marknjunge.iotclass.network;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;


public class FirebaseAuthUtils {
    private static FirebaseAuth firebaseAuth;

    static {
        initAuth();
    }

    private static void initAuth(){
        if (firebaseAuth == null)
            firebaseAuth = FirebaseAuth.getInstance();
    }

    public static FirebaseUser getCurrentUser() {
        return firebaseAuth.getCurrentUser();
    }

    public static String getCurrentUserId(){
        return "mmQo1NHP72dGjryYt1usIG3zWyt1";
    }

    public static void signOutUser() {
        if (getCurrentUser() != null)
            firebaseAuth.signOut();
    }

    public static void signIn(final String email, String password, final AuthTask authTask) {
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                authTask.taskCompleted(true, "Signed in successfully");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                authTask.taskCompleted(false, "Sign in failed: " + e.getMessage());
            }
        });
    }

//    public static void signInTemp(final AuthTask authTask) {
//        firebaseAuth.signInWithEmailAndPassword("testing@mail.com", "abc123").addOnSuccessListener(new OnSuccessListener<AuthResult>() {
//            @Override
//            public void onSuccess(AuthResult authResult) {
//                authTask.taskCompleted(true, "Signed in successfully");
//            }
//        }).addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                authTask.taskCompleted(false, "Sign in failed: " + e.getMessage());
//            }
//        });
//    }

    public static void sendPasswordResetEmail(String email, final AuthTask authTask) {
        firebaseAuth.sendPasswordResetEmail(email).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                authTask.taskCompleted(true, "Email sent");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                authTask.taskCompleted(false, "Sending failed: " + e.getMessage());
            }
        });
        authTask.taskCompleted(true, "Password reset email sent");
    }

    public static void createUser(String email, String password, final CreateUserListener createUserListener) {
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(AuthResult authResult) {
                createUserListener.taskCompleted(true, "User created successfully");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                createUserListener.taskCompleted(false, "User creation failed: " + e.getMessage());
            }
        });
    }

    public static void updateUserDisplayName(String displayName, final UpdateProfileListener updateProfileListener) {
        UserProfileChangeRequest changeRequest = new UserProfileChangeRequest.Builder()
                .setDisplayName(displayName)
                .build();

        getCurrentUser().updateProfile(changeRequest).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                updateProfileListener.updateCompleted(true, "Completed", getCurrentUser());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                updateProfileListener.updateCompleted(false, "Failed: " + e.getMessage(), getCurrentUser());
            }
        });
    }

    public interface AuthTask {
        void taskCompleted(boolean successful, String response);
    }

    public interface CreateUserListener {
        void taskCompleted(boolean successful, String response);
    }

    public interface UpdateProfileListener {
        void updateCompleted(boolean successful, String response, FirebaseUser user);
    }
}
