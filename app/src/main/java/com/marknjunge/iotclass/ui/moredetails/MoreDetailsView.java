package com.marknjunge.iotclass.ui.moredetails;

import com.github.mikephil.charting.data.Entry;

import java.util.List;

interface MoreDetailsView {
    void displayMessage(String message);
    void updateLastValue(String value, String time);
    void displayChartReadings(List<Entry> entries);
    void displayAnalysedData(String min, String max, String avg);
}
