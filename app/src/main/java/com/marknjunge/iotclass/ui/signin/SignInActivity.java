package com.marknjunge.iotclass.ui.signin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.marknjunge.iotclass.R;
import com.marknjunge.iotclass.ui.main.MainActivity;
import com.marknjunge.iotclass.ui.signup.SignUpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends AppCompatActivity implements SignInPresenter.SignInActivityView {

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.img_visibility)
    ImageView imgVisibility;
    @BindView(R.id.btn_sign_in)
    Button btnSignIn;
    @BindView(R.id.tv_forgot_pass)
    TextView tvForgotPass;
    @BindView(R.id.tv_sign_up)
    TextView tvSignUp;
    @BindView(R.id.pb_signing_in)
    ProgressBar pbSigningIn;


    private String email;
    private String password;
    private boolean passVisible;
    private SignInPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        passVisible = false;

        presenter = new SignInPresenter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            Intent intent = new Intent(SignInActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
    }

    @OnClick({R.id.img_visibility, R.id.btn_sign_in, R.id.tv_forgot_pass, R.id.tv_sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_visibility:
                if (passVisible)
                    setPasswordInvisible();
                else
                    setPasswordVisible();
                break;
            case R.id.btn_sign_in:
                if (validateFields())
                    signIn();
                break;
            case R.id.tv_forgot_pass:
                sendPasswordResetEmail();
                break;
            case R.id.tv_sign_up:
                startActivity(new Intent(SignInActivity.this, SignUpActivity.class));
                break;
        }
    }

    private void signIn() {
        disableButtons();
        presenter.signIn(email, password);
    }

    private void sendPasswordResetEmail() {
        email = etEmail.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            etEmail.setError("Enter your email address");
        } else {
            disableButtons();
            presenter.sendPasswordResetEmail(email);
        }
    }

    private void setPasswordVisible() {
        imgVisibility.setImageResource(R.drawable.ic_visible);
        etPassword.setTransformationMethod(null);
        passVisible = true;
    }

    private void setPasswordInvisible() {
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        imgVisibility.setImageResource(R.drawable.ic_invisible);
        passVisible = false;
    }

    private boolean validateFields() {
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            etEmail.setError("Required");
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            etPassword.setError("Required");
            return false;
        }

        return true;
    }

    private void disableButtons() {
        pbSigningIn.setVisibility(View.VISIBLE);
        btnSignIn.setEnabled(false);
        tvSignUp.setEnabled(false);
        tvForgotPass.setEnabled(false);
    }

    private void enableButtons() {
        pbSigningIn.setVisibility((View.INVISIBLE));
        btnSignIn.setEnabled(true);
        tvSignUp.setEnabled(true);
        tvForgotPass.setEnabled(true);
    }

    @Override
    public void goToMainActivity() {
        startActivity(new Intent(SignInActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void displayMessage(String message) {
        enableButtons();
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }
}
