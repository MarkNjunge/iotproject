package com.marknjunge.iotclass.network;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.marknjunge.iotclass.models.DataReading;
import com.marknjunge.iotclass.models.Section;
import com.marknjunge.iotclass.models.Sensor;

import java.util.List;

public interface DatabaseListener {
    void taskFailed(String message);
    void listenerReceived(DatabaseReference databaseReference, ValueEventListener eventListener);

    interface AnalysedDataListener extends DatabaseListener {
        void valuesReceived(String min, String max, String avg);
    }

    interface SensorsListener extends DatabaseListener {
        void valuesReceived(List<Sensor> sensors);
    }

    interface UserSectionsListener extends DatabaseListener {
        void valueReceived(List<Section> sections);
    }

    interface SingleValueListener extends DatabaseListener {
        void valuesReceived(String value, long timestamp);
    }

    interface DBValuesListener extends DatabaseListener {
        void valuesReceived(List<DataReading> values);
    }

    interface DBTask{
        void taskCompleted();
        void taskFailed(String message);
    }

}
