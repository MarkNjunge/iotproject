package com.marknjunge.iotclass.dagger;

import com.google.firebase.database.FirebaseDatabase;
import com.marknjunge.iotclass.network.DatabaseRepositoryImpl;
import com.marknjunge.iotclass.network.DatabaseRepository;

import dagger.Module;
import dagger.Provides;

@Module
class DatabaseModule {
    private FirebaseDatabase database;

    public DatabaseModule() {
        database = FirebaseDatabase.getInstance();
        database = FirebaseDatabase.getInstance();
        database.setPersistenceEnabled(true);
    }

    @Provides
    DatabaseRepository provideDatabaseRepository() {
        return new DatabaseRepositoryImpl(database);
    }

}
