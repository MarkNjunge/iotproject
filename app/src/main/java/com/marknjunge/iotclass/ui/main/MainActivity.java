package com.marknjunge.iotclass.ui.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.marknjunge.iotclass.IoTProjectApp;
import com.marknjunge.iotclass.R;
import com.marknjunge.iotclass.models.Section;
import com.marknjunge.iotclass.network.DatabaseRepository;
import com.marknjunge.iotclass.ui.sectionlanding.SectionLandingActivity;
import com.marknjunge.iotclass.utils.RandomUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.img_overflow_menu)
    ImageView imgOverflowMenu;
    @BindView(R.id.rv_sections)
    RecyclerView rvSections;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;

    private MainPresenter presenter;
    private MainActivityAdapter adapter;

    @Inject
    DatabaseRepository databaseRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

//        if (FirebaseAuthUtils.getCurrentUser() == null) {
//            startActivity(new Intent(this, SignInActivity.class));
//            finish();
//        }

        ((IoTProjectApp) getApplication()).getAppComponent().inject(this);

        rvSections.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        presenter = new MainPresenter(this, databaseRepository);
        presenter.getData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onActivityResumed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onActivityPaused();
    }

    @OnClick({R.id.img_overflow_menu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_overflow_menu:
                RandomUtils.displayOverflowMenu(this, imgOverflowMenu);
                break;
        }
    }

    @Override
    public void displaySections(List<Section> sections) {
        pbLoading.setVisibility(View.GONE);
        adapter = new MainActivityAdapter(this, sections);
        rvSections.setAdapter(adapter);
    }

    @Override
    public void displayNoSectionsAlert() {
        pbLoading.setVisibility(View.GONE);
        Toast.makeText(this, "You don't have any sections", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displayMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
