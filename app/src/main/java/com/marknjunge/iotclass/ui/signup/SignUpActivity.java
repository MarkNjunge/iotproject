package com.marknjunge.iotclass.ui.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.marknjunge.iotclass.IoTProjectApp;
import com.marknjunge.iotclass.R;
import com.marknjunge.iotclass.network.DatabaseRepository;
import com.marknjunge.iotclass.ui.main.MainActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity implements SignUpPresenter.SignUpActivityView {

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_pass)
    EditText etPass;
    @BindView(R.id.img_view_pass)
    ImageView imgViewPass;
    @BindView(R.id.et_pass_repeat)
    EditText etPassRepeat;
    @BindView(R.id.img_view_pass_rpt)
    ImageView imgViewPassRpt;
    @BindView(R.id.btn_sign_up)
    Button btnSignUp;
    @BindView(R.id.pb_signing_up)
    ProgressBar pbSigningUp;
    @BindView(R.id.tv_sign_in)
    TextView tvSignIn;

    private SignUpPresenter presenter;
    private String name;
    private String email;
    private String password;
    private boolean passVisible;
    private boolean passRptVisible;

    @Inject
    DatabaseRepository databaseRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        ((IoTProjectApp) getApplication()).getAppComponent().inject(this);
        presenter = new SignUpPresenter(this, databaseRepository);
        passVisible = false;
        passRptVisible = false;
    }

    @OnClick({R.id.img_view_pass, R.id.img_view_pass_rpt, R.id.btn_sign_up, R.id.tv_sign_in})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_view_pass:
                if (passVisible) {
                    setPasswordInvisible();
                } else {
                    setPasswordVisible();
                }
                break;
            case R.id.img_view_pass_rpt:
                if (passRptVisible) {
                    setPasswordRptInvisible();
                } else {
                    setPasswordRptVisible();
                }
                break;
            case R.id.btn_sign_up:
                if (validateFields())
                    createUser();
                break;
            case R.id.tv_sign_in:
                goToSignIn();
                break;
        }
    }

    private void createUser() {
        disableButtons();
        presenter.createUser(name, email, password);
    }

    private void setPasswordVisible() {
        etPass.setTransformationMethod(null);
        imgViewPass.setImageResource(R.drawable.ic_visible);
        passVisible = true;
    }

    private void setPasswordRptVisible() {
        etPassRepeat.setTransformationMethod(null);
        imgViewPassRpt.setImageResource(R.drawable.ic_visible);
        passRptVisible = true;
    }

    private void setPasswordInvisible() {
        etPass.setTransformationMethod(new PasswordTransformationMethod());
        imgViewPass.setImageResource(R.drawable.ic_invisible);
        passVisible = false;
    }

    private void setPasswordRptInvisible() {
        etPassRepeat.setTransformationMethod(new PasswordTransformationMethod());
        imgViewPassRpt.setImageResource(R.drawable.ic_invisible);
        passRptVisible = false;
    }

    private boolean validateFields() {
        name = etName.getText().toString().trim();
        email = etEmail.getText().toString().trim();
        password = etPass.getText().toString().trim();
        String passwordRpt = etPassRepeat.getText().toString().trim();

        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || TextUtils.isEmpty(passwordRpt)) {
            Toast.makeText(this, "All fields are required", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError("Incorrect format");
            return false;
        }
        if (password.length() < 6) {
            etPass.setError("At least 6 characters");
            return false;
        }
        if (!TextUtils.equals(password, passwordRpt)) {
            etPassRepeat.setError("Passwords do no match");
            return false;
        }
        return true;
    }

    private void disableButtons() {
        pbSigningUp.setVisibility(View.VISIBLE);
        btnSignUp.setEnabled(false);
    }

    private void enableButtons() {
        pbSigningUp.setVisibility(View.INVISIBLE);
        btnSignUp.setEnabled(true);
    }

    private void goToSignIn() {
        finish();
    }

    @Override
    public void goToMainActivity() {
        Toast.makeText(SignUpActivity.this, "User created successfully", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
        // Because the SignInActivity will still be in the back stack
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void displayMessage(String message) {
        enableButtons();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
