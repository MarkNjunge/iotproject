package com.marknjunge.iotclass.network;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public interface DatabaseRepository {
    void getLastValue(String section, String sensor, final DatabaseListener.SingleValueListener listener);

    void getValues(String section, String sensor, final int count, final DatabaseListener.DBValuesListener valuesListener);

    void getAnalysedData(String section, String sensor, final DatabaseListener.AnalysedDataListener dataListener);

    void getUserSections(final DatabaseListener.UserSectionsListener sectionsListener);

    void getUserSensors(final String section, final DatabaseListener.SensorsListener sensorsListener);

    void addUserDetails(FirebaseUser user, String name, String email, final DatabaseListener.DBTask dbTask);

    void attachListener(DatabaseReference databaseReference, ValueEventListener valueEventListener);

    void detachListener(DatabaseReference databaseReference, ValueEventListener valueEventListener);
}
