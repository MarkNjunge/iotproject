package com.marknjunge.iotclass.models;

public class DataReading {
    private String value;
    private long timestamp;

    public DataReading(String value, long timestamp) {
        this.value = value;
        this.timestamp = timestamp;
    }

    public String getValue() {
        return value;
    }

    public long getTimestamp() {
        return timestamp;
    }
}
