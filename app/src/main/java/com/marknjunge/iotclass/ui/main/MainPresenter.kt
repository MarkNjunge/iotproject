package com.marknjunge.iotclass.ui.main

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.marknjunge.iotclass.models.Section
import com.marknjunge.iotclass.network.DatabaseListener
import com.marknjunge.iotclass.network.DatabaseRepository

internal class MainPresenter(private val view: MainView, private val databaseRepository: DatabaseRepository) {
    private var mDatabaseReference: DatabaseReference? = null
    private var mEventListener: ValueEventListener? = null
    private var listenerWasDetached: Boolean = false

    fun getData() {
        databaseRepository.getUserSections(object : DatabaseListener.UserSectionsListener {
            override fun listenerReceived(databaseReference: DatabaseReference?, eventListener: ValueEventListener?) {
                mDatabaseReference = databaseReference
                mEventListener = eventListener
            }

            override fun valueReceived(sections: List<Section>) {
                if (sections.isEmpty()) {
                    view.displayNoSectionsAlert()
                } else {
                    view.displaySections(sections)
                }
            }

            override fun taskFailed(message: String) {
                view.displayMessage(message)
            }
        })

    }

    fun onActivityResumed() {
        if (listenerWasDetached) {
            databaseRepository.attachListener(mDatabaseReference, mEventListener)
        }
    }

    fun onActivityPaused() {
        databaseRepository.detachListener(mDatabaseReference, mEventListener)
        listenerWasDetached = true
    }

}
