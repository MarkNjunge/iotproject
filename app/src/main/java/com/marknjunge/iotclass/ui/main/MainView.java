package com.marknjunge.iotclass.ui.main;

import com.marknjunge.iotclass.models.Section;

import java.util.List;

interface MainView {
    void displaySections(List<Section> sections);
    void displayNoSectionsAlert();

    void displayMessage(String message);
}
