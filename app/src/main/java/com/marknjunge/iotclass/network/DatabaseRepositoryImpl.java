package com.marknjunge.iotclass.network;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.marknjunge.iotclass.models.DataReading;
import com.marknjunge.iotclass.models.Section;
import com.marknjunge.iotclass.models.Sensor;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class DatabaseRepositoryImpl implements DatabaseRepository {
    //    private static final String USER = "mmQo1NHP72dGjryYt1usIG3zWyt1";
    private static DatabaseReference databaseReference;

    public DatabaseRepositoryImpl(FirebaseDatabase database) {
        databaseReference = database.getReference();
    }

    @Override
    public void getLastValue(String section, String sensor, final DatabaseListener.SingleValueListener listener) {
        String path = "data/" + FirebaseAuthUtils.getCurrentUserId() + "/" + section + "/" + sensor;
        Query query = databaseReference.child(path).limitToLast(1);

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    listener.valuesReceived(String.valueOf(data.child("value").getValue()), (Long) data.child("timestamp").getValue());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                listener.taskFailed(databaseError.getMessage());
            }
        };

        query.addListenerForSingleValueEvent(valueEventListener);
        listener.listenerReceived(query.getRef(), valueEventListener);
    }

    @Override
    public void getValues(final String section, final String sensor, int count, final DatabaseListener.DBValuesListener valuesListener) {
        String path = "data/" + FirebaseAuthUtils.getCurrentUserId() + "/" + section + "/" + sensor;
        Query query = databaseReference.child(path).limitToLast(count);

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Timber.d("data/" + FirebaseAuthUtils.getCurrentUserId() + "/" + section + "/" + sensor);
                List<DataReading> dataReadings = new ArrayList<>();

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    dataReadings.add(
                            new DataReading(
                                    data.child("value").getValue().toString(),
                                    (long) data.child("timestamp").getValue()
                            )
                    );
                }

                valuesListener.valuesReceived(dataReadings);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                valuesListener.taskFailed(databaseError.getMessage());
            }
        };

        query.addValueEventListener(valueEventListener);
        valuesListener.listenerReceived(query.getRef(), valueEventListener);
    }

    @Override
    public void getAnalysedData(String section, String sensor, final DatabaseListener.AnalysedDataListener dataListener) {
        String path = String.format("analysedData/%s/%s/%s", FirebaseAuthUtils.getCurrentUserId(), section, sensor);
        Query query = databaseReference.child(path);

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    String min = dataSnapshot.child("min").getValue().toString();
                    String max = dataSnapshot.child("max").getValue().toString();
                    String avg = dataSnapshot.child("average").getValue().toString();
                    dataListener.valuesReceived(min, max, avg);
                } else {
                    dataListener.taskFailed("There is no analysed data");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dataListener.taskFailed(databaseError.getMessage());
            }
        };

        query.addValueEventListener(valueEventListener);
        dataListener.listenerReceived(query.getRef(), valueEventListener);
    }

    @Override
    public void getUserSections(final DatabaseListener.UserSectionsListener sectionsListener) {
//        String path = "sections/" + FirebaseAuthUtils.getCurrentUserId();
        String path = "data/" + FirebaseAuthUtils.getCurrentUserId();
        Query query = databaseReference.child(path);

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Timber.d(dataSnapshot.toString());
                List<Section> sections = new ArrayList<>();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    List<String> sensors = new ArrayList<>();
                    sensors.add(snapshot.getKey());
                    sections.add(new Section(snapshot.getKey(), sensors));
                }

                sectionsListener.valueReceived(sections);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                sectionsListener.taskFailed(databaseError.getMessage());
            }
        };

        query.addValueEventListener(valueEventListener);
        sectionsListener.listenerReceived(query.getRef(), valueEventListener);
    }

    @Override
    public void getUserSensors(final String section, final DatabaseListener.SensorsListener sensorsListener) {
        String databasePath = String.format("sections/%s/%s", FirebaseAuthUtils.getCurrentUserId(), section);
        DatabaseReference dbRef = databaseReference.child(databasePath);

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Sensor> sensors = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if (!snapshot.getKey().equals("gateway")) {
                        String type;
                        if (snapshot.getKey().startsWith("C")) {
                            type = Sensor.SENSOR_CURRENT;
                        } else if (snapshot.getKey().startsWith("L")) {
                            type = Sensor.SENSOR_LIGHT;
                        } else if (snapshot.getKey().startsWith("T")) {
                            type = Sensor.SENSOR_TEMPERATURE;
                        } else if (snapshot.getKey().startsWith("H")) {
                            type = Sensor.SENSOR_HUMIDITY;
                        } else {
                            type = "unspecified";
                        }
                        sensors.add(
                                new Sensor()
                                        .setSensorID(snapshot.getKey())
                                        .setType(type)
                                        .setName((String) snapshot.child("name").getValue())
                                        .setLastReading(String.valueOf(snapshot.child("lastReading").getValue()))
                                        .setLastTimestamp((Long) snapshot.child("lastTimestamp").getValue())
                        );
                    }
                }
                sensorsListener.valuesReceived(sensors);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                sensorsListener.taskFailed(databaseError.getMessage());
            }
        };

        dbRef.addValueEventListener(valueEventListener);
        sensorsListener.listenerReceived(dbRef, valueEventListener);
    }

    @Override
    public void addUserDetails(FirebaseUser user, String name, String email, final DatabaseListener.DBTask dbTask) {
        DatabaseReference dbRef = databaseReference.child("users/" + user.getUid());
        dbRef.child("name").setValue(name);
        dbRef.child("email").setValue(email);

        DatabaseReference newUserRef = databaseReference.child("users/" + user.getUid());

        newUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild("name")) {
                    dbTask.taskCompleted();
                } else {
                    dbTask.taskFailed("Error adding user details");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dbTask.taskFailed(databaseError.getMessage());
            }
        });
    }

    @Override
    public void attachListener(DatabaseReference databaseReference, ValueEventListener valueEventListener) {
        databaseReference.addValueEventListener(valueEventListener);
    }

    @Override
    public void detachListener(DatabaseReference databaseReference, ValueEventListener valueEventListener) {
        databaseReference.removeEventListener(valueEventListener);
    }

}
