package com.marknjunge.iotclass;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.marknjunge.iotclass.ui.main.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class MainActivityActivityTest {

    @Rule
    public final ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void shouldTraverseAppActivities() {
        // MainActivity is displayed
        onView(withId(R.id.img_overflow_menu)).check(ViewAssertions.matches(isDisplayed()));
        onView(withId(R.id.rv_sections)).check(ViewAssertions.matches(isDisplayed()));

        // Open SectionLandingActivity
        onView(withId(R.id.rv_sections)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        // SectionLandingActivity is displayed
        onView(withId(R.id.rv_sensors)).check(ViewAssertions.matches(isDisplayed()));

        // Open MoreDetailsActivity
        onView(withId(R.id.rv_sensors)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        // MoreDetailsActivity is displayed
        onView(withId(R.id.line_chart)).check(ViewAssertions.matches(isDisplayed()));

        // Go back up to SectionLandingActivity
        onView(withId(R.id.img_back)).perform(click());
        onView(withId(R.id.rv_sensors)).check(ViewAssertions.matches(isDisplayed()));

        // Go back up to MainActivity
        onView(withId(R.id.img_back)).perform(click());
        onView(withId(R.id.rv_sections)).check(ViewAssertions.matches(isDisplayed()));
    }
}
