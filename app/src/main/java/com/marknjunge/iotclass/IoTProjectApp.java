package com.marknjunge.iotclass;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.marknjunge.iotclass.dagger.AppComponent;
import com.marknjunge.iotclass.dagger.DaggerAppComponent;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class IoTProjectApp extends Application {
    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree() {
                @Override
                protected String createStackElementTag(StackTraceElement element) {
                    return "Timber: " + super.createStackElementTag(element) + " " + element.getMethodName();
                }
            });
            Fabric fabric = new Fabric.Builder(this).debuggable(true).build();
            Fabric.with(fabric);
        } else {
            Fabric.with(this, new Crashlytics());
        }

        appComponent = DaggerAppComponent.builder().build();
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }
}
