package com.marknjunge.iotclass.ui.moredetails;

import com.github.mikephil.charting.data.Entry;
import com.marknjunge.iotclass.MockDatabase;
import com.marknjunge.iotclass.models.Sensor;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class MoreDetailsPresenterTest {

    private MockActivity mockActivity;

    @Before
    public void setup() {
        mockActivity = new MockActivity();

        Sensor sensor = new Sensor("id", "name", "lastReading", 123456L);
        MockDatabase databaseRepository = new MockDatabase();

        MoreDetailsPresenter presenter = new MoreDetailsPresenter(mockActivity, databaseRepository, sensor, "section");
        presenter.getData();

    }

    @Test
    public void shouldDisplayMessage(){
        Assert.assertEquals(true, mockActivity.messageDisplayed);
    }

    @Test
    public void shouldUpdateLastValue(){
        Assert.assertEquals(true, mockActivity.lastValuesUpdated);
    }

    @Test
    public void shouldDisplayChartReadings(){
        Assert.assertEquals(true, mockActivity.chartReadingsDisplayed);
    }

    @Test
    public void shouldDisplayAnalysedData(){
        Assert.assertEquals(true, mockActivity.analysedDataDisplayed);
    }


    private class MockActivity implements MoreDetailsView {
        private boolean messageDisplayed;
        private boolean lastValuesUpdated;
        private boolean chartReadingsDisplayed;
        private boolean analysedDataDisplayed;

        @Override
        public void displayMessage(String message) {
            messageDisplayed = true;
        }

        @Override
        public void updateLastValue(String value, String time) {
            lastValuesUpdated = true;
        }

        @Override
        public void displayChartReadings(List<Entry> entries) {
            chartReadingsDisplayed = true;
        }

        @Override
        public void displayAnalysedData(String min, String max, String avg) {
            analysedDataDisplayed = true;
        }
    }
}