package com.marknjunge.iotclass.ui.sectionlanding;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.marknjunge.iotclass.R;
import com.marknjunge.iotclass.models.Sensor;
import com.marknjunge.iotclass.ui.moredetails.MoreDetailsActivity;
import com.marknjunge.iotclass.utils.RandomUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class SectionLandingAdapter extends RecyclerView.Adapter<SectionLandingAdapter.ViewHolder> {

    @BindView(R.id.tv_sensor_name)
    TextView tvSensorName;
    @BindView(R.id.tv_last_reading)
    TextView tvLastReading;
    @BindView(R.id.tv_last_reading_time)
    TextView tvLastReadingTime;
    @BindView(R.id.tv_last_reading_label)
    TextView tvLastReadingLabel;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.img_sensor_icon)
    ImageView imgSensorIcon;
    @BindView(R.id.root_layout)
    CardView rootLayout;

    private Context context;
    private List<Sensor> sensors;
    private String section;

    SectionLandingAdapter(Context context, List<Sensor> sensors, String section) {
        this.context = context;
        this.sensors = sensors;
        this.section = section;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_sensor, parent, false);
        ButterKnife.bind(this, view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        pbLoading.setVisibility(View.GONE);
        final Sensor sensor = sensors.get(position);

        tvSensorName.setText(sensor.getName());
        tvLastReading.setText(sensor.getLastReading() + sensor.getUnits());
        tvLastReadingTime.setText(RandomUtils.getReadableTime(sensor.getLastTimestamp()));

        String type = sensor.getType();
        switch (type) {
            case Sensor.SENSOR_CURRENT:
                imgSensorIcon.setImageResource(R.drawable.ic_current);
                break;
            case Sensor.SENSOR_LIGHT:
                imgSensorIcon.setImageResource(R.drawable.ic_light);
                break;
            case Sensor.SENSOR_TEMPERATURE:
                imgSensorIcon.setImageResource(R.drawable.ic_temperature);
                break;
            case Sensor.SENSOR_HUMIDITY:
                imgSensorIcon.setImageResource(R.drawable.ic_humidity);
                break;
            default:
                imgSensorIcon.setImageResource(R.drawable.ic_iot_logo_plain);
                break;
        }

        rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MoreDetailsActivity.start(context, sensor, section);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sensors.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
