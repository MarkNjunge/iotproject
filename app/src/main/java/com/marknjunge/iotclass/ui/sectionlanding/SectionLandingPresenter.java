package com.marknjunge.iotclass.ui.sectionlanding;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.marknjunge.iotclass.models.Sensor;
import com.marknjunge.iotclass.network.DatabaseListener;
import com.marknjunge.iotclass.network.DatabaseRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class SectionLandingPresenter {
    private SectionLandingView activityView;
    private DatabaseRepository databaseRepository;
    private String section;
    private Map<DatabaseReference, ValueEventListener> eventListenerMap;

    SectionLandingPresenter(SectionLandingView activityView, DatabaseRepository databaseRepository, String section) {
        this.activityView = activityView;
        this.databaseRepository = databaseRepository;
        this.section = section;
        eventListenerMap = new HashMap<>();
    }

    void getUserSensors(){
        DatabaseListener.SensorsListener sensorsListener = new DatabaseListener.SensorsListener() {
            @Override
            public void valuesReceived(List<Sensor> sensors) {
                if (sensors.isEmpty()) {
                    activityView.displayNoSensorsText();
                } else {
                    activityView.displaySensors(sensors);
                }
            }

            @Override
            public void taskFailed(String message) {
                activityView.displayMessage(message);
            }

            @Override
            public void listenerReceived(DatabaseReference databaseReference, ValueEventListener eventListener) {
                eventListenerMap.put(databaseReference, eventListener);
            }
        };

        databaseRepository.getUserSensors(section, sensorsListener);
    }

    void activityPaused() {
        for (Map.Entry<DatabaseReference, ValueEventListener> entry : eventListenerMap.entrySet()) {
            databaseRepository.detachListener(entry.getKey(), entry.getValue());
        }
    }
}
