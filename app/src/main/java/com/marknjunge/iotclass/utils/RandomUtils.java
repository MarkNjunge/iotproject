package com.marknjunge.iotclass.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.marknjunge.iotclass.R;
import com.marknjunge.iotclass.network.FirebaseAuthUtils;
import com.marknjunge.iotclass.ui.about.AboutActivity;
import com.marknjunge.iotclass.ui.signin.SignInActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class RandomUtils {
    public static String getReadableTime(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(timestamp);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss - E, dd/MM/yy");
            Date currentTime = calendar.getTime();
            return sdf.format(currentTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(timestamp);
    }

    public static void displayOverflowMenu(final Context context, View anchor) {
        PopupMenu popupMenu = new PopupMenu(context, anchor);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.menu_overflow, popupMenu.getMenu());
        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_about:
                        context.startActivity(new Intent(context, AboutActivity.class));
                        break;
                    case R.id.menu_sign_out:
                        FirebaseAuthUtils.signOutUser();
                        Intent intent = new Intent(context, SignInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        break;
                }
                return false;
            }
        });
    }

}
