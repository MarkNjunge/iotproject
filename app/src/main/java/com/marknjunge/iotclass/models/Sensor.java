package com.marknjunge.iotclass.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Sensor implements Parcelable {
    private String sensorID;
    private String name;
    private String type;
    private String lastReading;
    private long lastTimestamp;

    public static final String SENSOR_CURRENT = "Current";
    public static final String SENSOR_LIGHT = "Light";
    public static final String SENSOR_TEMPERATURE = "Temperature";
    public static final String SENSOR_HUMIDITY = "Humidity";

    public Sensor() {
    }

    public Sensor(String sensorID, String name, String lastReading, long lastTimestamp) {
        this.sensorID = sensorID;
        this.name = name;
        this.lastReading = lastReading;
        this.lastTimestamp = lastTimestamp;
    }

    public Sensor setSensorID(String sensorID) {
        this.sensorID = sensorID;
        return this;
    }

    public Sensor setName(String name) {
        this.name = name;
        return this;
    }

    public Sensor setType(String type) {
        this.type = type;
        return this;
    }

    public Sensor setLastReading(String lastReading) {
        this.lastReading = lastReading;
        return this;
    }

    public Sensor setLastTimestamp(long lastTimestamp) {
        this.lastTimestamp = lastTimestamp;
        return this;
    }

    public String getSensorID() {
        return sensorID;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getLastReading() {
        return lastReading;
    }

    public long getLastTimestamp() {
        return lastTimestamp;
    }

    public String getUnits() {
        switch (this.getType()) {
            case Sensor.SENSOR_LIGHT:
                return " LUX";
            case Sensor.SENSOR_TEMPERATURE:
                return "°C";
            case Sensor.SENSOR_HUMIDITY:
                return "%";
            case Sensor.SENSOR_CURRENT:
                return "A";
            default:
                return "";
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sensorID);
        dest.writeString(this.name);
        dest.writeString(this.type);
        dest.writeString(this.lastReading);
        dest.writeLong(this.lastTimestamp);
    }

    protected Sensor(Parcel in) {
        this.sensorID = in.readString();
        this.name = in.readString();
        this.type = in.readString();
        this.lastReading = in.readString();
        this.lastTimestamp = in.readLong();
    }

    public static final Creator<Sensor> CREATOR = new Creator<Sensor>() {
        @Override
        public Sensor createFromParcel(Parcel source) {
            return new Sensor(source);
        }

        @Override
        public Sensor[] newArray(int size) {
            return new Sensor[size];
        }
    };
}
