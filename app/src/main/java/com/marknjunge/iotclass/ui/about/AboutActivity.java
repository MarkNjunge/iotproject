package com.marknjunge.iotclass.ui.about;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.marknjunge.iotclass.BuildConfig;
import com.marknjunge.iotclass.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AboutActivity extends AppCompatActivity{

    @BindView(R.id.tv_version_number)
    TextView tvVersionNumber;
    @BindView(R.id.img_github)
    ImageView imgGithub;
    @BindView(R.id.img_linkedin)
    ImageView imgLinkedin;
    @BindView(R.id.img_mail)
    ImageView imgMail;
    @BindView(R.id.img_open_chart)
    ImageView imgOpenChart;
    @BindView(R.id.img_back)
    ImageView imgBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);

        setVersionNumber();
    }

    @OnClick({R.id.img_github, R.id.img_linkedin, R.id.img_mail, R.id.img_open_chart, R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_github:
                openGithub();
                break;
            case R.id.img_linkedin:
                openLinkedIn();
                break;
            case R.id.img_mail:
                sendEmail();
                break;
            case R.id.img_open_chart:
                openChartLibrary();
                break;
            case R.id.img_back:
                closeActivity();
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    private void setVersionNumber() {
        PackageInfo pInfo;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            tvVersionNumber.setText("V " + pInfo.versionName);
            if (BuildConfig.DEBUG)
                tvVersionNumber.append(" (debug)");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openGithub() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/MarkNjunge")));
    }

    private void openLinkedIn() {
        Toast.makeText(this, "linkedin.com/in/marknkamau", Toast.LENGTH_LONG).show();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://linkedin.com/in/marknjunge")));
    }

    private void sendEmail() {
        String[] addresses = {"mark.kamau@outlook.com"}; //Has to be String array or it will ignore
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        startActivity(intent);
    }

    private void openChartLibrary() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://github.com/PhilJay/MPAndroidChart")));
    }

    private void closeActivity() {
        finish();
    }
}
