package com.marknjunge.iotclass.ui.signup;

import com.google.firebase.auth.FirebaseUser;
import com.marknjunge.iotclass.network.DatabaseListener;
import com.marknjunge.iotclass.network.DatabaseRepository;
import com.marknjunge.iotclass.network.FirebaseAuthUtils;

class SignUpPresenter {

    private SignUpActivityView activityView;
    private DatabaseRepository databaseRepository;

    SignUpPresenter(SignUpActivityView activityView, DatabaseRepository databaseRepository) {
        this.activityView = activityView;
        this.databaseRepository = databaseRepository;
    }

    void createUser(final String name, final String email, final String password) {
        FirebaseAuthUtils.createUser(email, password, new FirebaseAuthUtils.CreateUserListener() {
            @Override
            public void taskCompleted(boolean successful, String response) {
                if (successful) {
                    signInUser(email, password, name);
                } else {
                    activityView.displayMessage(response);
                }
            }
        });
    }

    private void signInUser(final String email, String password, final String name) {
        FirebaseAuthUtils.signIn(email, password, new FirebaseAuthUtils.AuthTask() {
            @Override
            public void taskCompleted(boolean successful, String response) {
                if (successful) {
                    updateUserDisplayName(name, email);
                } else {
                    activityView.displayMessage(response);
                }
            }
        });
    }

    private void updateUserDisplayName(final String name, final String email) {
        FirebaseAuthUtils.updateUserDisplayName(name, new FirebaseAuthUtils.UpdateProfileListener() {
            @Override
            public void updateCompleted(boolean successful, String response, FirebaseUser user) {
                if (successful) {
                    databaseRepository.addUserDetails(user, name, email, new DatabaseListener.DBTask() {

                        @Override
                        public void taskCompleted() {
                            activityView.goToMainActivity();
                        }

                        @Override
                        public void taskFailed(String message) {
                            activityView.displayMessage("Something went wrong");
                        }

                    });
                } else {
                    activityView.displayMessage(response);
                }
            }
        });
    }

    interface SignUpActivityView {
        void goToMainActivity();

        void displayMessage(String message);
    }
}
