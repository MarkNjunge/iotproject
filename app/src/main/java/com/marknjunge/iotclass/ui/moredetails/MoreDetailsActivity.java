package com.marknjunge.iotclass.ui.moredetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.marknjunge.iotclass.IoTProjectApp;
import com.marknjunge.iotclass.R;
import com.marknjunge.iotclass.models.Sensor;
import com.marknjunge.iotclass.network.DatabaseRepository;
import com.marknjunge.iotclass.ui.about.AboutActivity;
import com.marknjunge.iotclass.ui.signin.SignInActivity;
import com.marknjunge.iotclass.network.FirebaseAuthUtils;
import com.marknjunge.iotclass.utils.MyLineChart;
import com.marknjunge.iotclass.utils.RandomUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MoreDetailsActivity extends AppCompatActivity implements MoreDetailsView {

    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_sensor_name)
    TextView tvSensorName;
    @BindView(R.id.tv_last_value)
    TextView tvLastValue;
    @BindView(R.id.tv_last_value_time)
    TextView tvLastValueTime;
    @BindView(R.id.tv_max_value)
    TextView tvMaxValue;
    @BindView(R.id.tv_min_value)
    TextView tvMinValue;
    @BindView(R.id.tv_avg_value)
    TextView tvAvgValue;
    @BindView(R.id.line_chart)
    LineChart lineChart;
    @BindView(R.id.img_overflow_menu)
    ImageView imgOverflowMenu;

    @Inject
    DatabaseRepository databaseRepository;

    private static final String SENSOR_KEY = "sensor_key";
    private static final String SECTION = "section";
    private MyLineChart chart;
    private MoreDetailsPresenter presenter;
    private Sensor sensor;

    public static void start(Context context, Sensor sensor, String section) {
        Intent starter = new Intent(context, MoreDetailsActivity.class);
        starter.putExtra(SENSOR_KEY, sensor);
        starter.putExtra(SECTION, section);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_details);
        ButterKnife.bind(this);

        ((IoTProjectApp) getApplication()).getAppComponent().inject(this);

        sensor = getIntent().getExtras().getParcelable(SENSOR_KEY);
        String section = getIntent().getStringExtra(SECTION);

        if (sensor != null) {
            tvSensorName.setText(sensor.getName());
//            tvLastValue.setText(sensor.getLastReading() + sensor.getUnits());
//            tvLastValueTime.setText(RandomUtils.getReadableTime(sensor.getLastTimestamp()));
        }

        chart = new MyLineChart(this, lineChart);
        chart.initializeChart();

        presenter = new MoreDetailsPresenter(this, databaseRepository, sensor, section);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getData();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.activityPaused();
    }

    @OnClick({R.id.img_back, R.id.img_overflow_menu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.img_overflow_menu:
                displayOverflowMenu();
                break;
        }
    }

    @Override
    public void displayMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void updateLastValue(String value, String time) {
        tvLastValue.setText(value + sensor.getUnits());
        tvLastValueTime.setText(time);
    }

    @Override
    public void displayChartReadings(List<Entry> entries) {
        chart.setChartValues(entries);
    }

    @Override
    public void displayAnalysedData(String min, String max, String avg) {
        tvMinValue.setText(min + sensor.getUnits());
        tvMaxValue.setText(max + sensor.getUnits());
        tvAvgValue.setText(avg + sensor.getUnits());
    }

    private void displayOverflowMenu() {
        PopupMenu popupMenu = new PopupMenu(this, imgOverflowMenu);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.menu_overflow, popupMenu.getMenu());
        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_about:
                        startActivity(new Intent(MoreDetailsActivity.this, AboutActivity.class));
                        break;
                    case R.id.menu_sign_out:
                        FirebaseAuthUtils.signOutUser();
                        Intent intent = new Intent(MoreDetailsActivity.this, SignInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                        break;
                }
                return false;
            }
        });
    }
}
