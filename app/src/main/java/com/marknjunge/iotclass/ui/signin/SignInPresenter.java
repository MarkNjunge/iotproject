package com.marknjunge.iotclass.ui.signin;

import com.marknjunge.iotclass.network.FirebaseAuthUtils;

class SignInPresenter {

    private SignInActivityView activityView;

    SignInPresenter(SignInActivityView activityView) {
        this.activityView = activityView;
    }

    void signIn(String email, String password) {
        FirebaseAuthUtils.signIn(email, password, new FirebaseAuthUtils.AuthTask() {
            @Override
            public void taskCompleted(boolean successful, String response) {
                if (successful) {
                    activityView.goToMainActivity();
                } else {
                    activityView.displayMessage(response);
                }
            }
        });
    }

//    void signInTemp(){
//        FirebaseAuthUtils.signInTemp(new FirebaseAuthUtils.AuthTask() {
//            @Override
//            public void taskCompleted(boolean successful, String response) {
//                if (successful) {
//                    activityView.goToMainActivity();
//                } else {
//                    activityView.displayMessage(response);
//                }
//            }
//        });
//    }

    void sendPasswordResetEmail(String email) {
        FirebaseAuthUtils.sendPasswordResetEmail(email, new FirebaseAuthUtils.AuthTask() {
            @Override
            public void taskCompleted(boolean successful, String response) {
                activityView.displayMessage(response);
            }
        });
    }

    interface SignInActivityView {
        void goToMainActivity();

        void displayMessage(String message);
    }

}
