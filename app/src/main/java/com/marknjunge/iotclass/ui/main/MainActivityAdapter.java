package com.marknjunge.iotclass.ui.main;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.marknjunge.iotclass.R;
import com.marknjunge.iotclass.models.Section;
import com.marknjunge.iotclass.ui.sectionlanding.SectionLandingActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class MainActivityAdapter extends RecyclerView.Adapter<MainActivityAdapter.ViewHolder> {
    @BindView(R.id.anim_section_icon)
    LottieAnimationView animSectionIcon;
    @BindView(R.id.tv_section_name)
    TextView tvSectionName;

    private final Context context;
    @BindView(R.id.card_item)
    CardView cardItem;
    private List<Section> sections;

    MainActivityAdapter(Context context, List<Section> sections) {
        this.context = context;
        this.sections = sections;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_section, parent, false);
        ButterKnife.bind(this, view);

        animSectionIcon.loop(true);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Section section = sections.get(position);

        final String sectionName = section.getSectionName();
        tvSectionName.setText(sectionName);

        if (sectionName.equals("home")) {
            animSectionIcon.setImageResource(R.drawable.ic_home);
        } else if (sectionName.equals("agriculture")) {
            animSectionIcon.setAnimation("farm.json");
            animSectionIcon.playAnimation();
        }

        cardItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SectionLandingActivity.start(context, sectionName);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sections.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
