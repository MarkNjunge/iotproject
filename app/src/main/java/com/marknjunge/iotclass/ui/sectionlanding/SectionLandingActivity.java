package com.marknjunge.iotclass.ui.sectionlanding;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.perf.metrics.AddTrace;
import com.marknjunge.iotclass.IoTProjectApp;
import com.marknjunge.iotclass.R;
import com.marknjunge.iotclass.models.Sensor;
import com.marknjunge.iotclass.network.DatabaseRepository;
import com.marknjunge.iotclass.ui.about.AboutActivity;
import com.marknjunge.iotclass.ui.signin.SignInActivity;
import com.marknjunge.iotclass.network.FirebaseAuthUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SectionLandingActivity extends AppCompatActivity implements SectionLandingView {

    @BindView(R.id.tv_section_name)
    TextView tvSectionName;
    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;
    @BindView(R.id.rv_sensors)
    RecyclerView rvSensors;
    @BindView(R.id.tv_no_sensors)
    TextView tvNoSensors;
    @BindView(R.id.img_overflow_menu)
    ImageView imgOverflowMenu;

    private static final String SECTION_NAME = "sections_name";
    private String section;
    private SectionLandingPresenter presenter;

    @Inject
    DatabaseRepository databaseRepository;

    public static void start(Context context, String section) {
        Intent starter = new Intent(context, SectionLandingActivity.class);
        starter.putExtra(SECTION_NAME, section);
        context.startActivity(starter);
    }

    @Override
    @AddTrace(name = "sectionLandingCreated")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section_landing);
        ButterKnife.bind(this);

        ((IoTProjectApp) getApplication()).getAppComponent().inject(this);
        rvSensors.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        section = getIntent().getStringExtra(SECTION_NAME);

        tvSectionName.setText(section);

//        if (TextUtils.equals(section, "home")) {
//            tvSectionName.setText(getString(R.string.home));
//        } else if (TextUtils.equals(section, "agriculture")) {
//            tvSectionName.setText(getString(R.string.agriculture));
//        }

        presenter = new SectionLandingPresenter(this, databaseRepository, section);
        presenter.getUserSensors();

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getUserSensors();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.activityPaused();
    }

    @OnClick({R.id.img_back, R.id.img_overflow_menu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.img_overflow_menu:
                displayOverflowMenu();
                break;
        }
    }

    @Override
    public void displayMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void displaySensors(List<Sensor> sensors) {
        pbLoading.setVisibility(View.GONE);
        SectionLandingAdapter adapter = new SectionLandingAdapter(this, sensors, section);
        rvSensors.setAdapter(adapter);
    }

    @Override
    public void displayNoSensorsText() {
        pbLoading.setVisibility(View.GONE);
        tvNoSensors.setVisibility(View.VISIBLE);
    }

    private void displayOverflowMenu() {
        PopupMenu popupMenu = new PopupMenu(this, imgOverflowMenu);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.menu_overflow, popupMenu.getMenu());
        popupMenu.show();

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_about:
                        startActivity(new Intent(SectionLandingActivity.this, AboutActivity.class));
                        break;
                    case R.id.menu_sign_out:
                        FirebaseAuthUtils.signOutUser();
                        Intent intent = new Intent(SectionLandingActivity.this, SignInActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                        break;
                }
                return false;
            }
        });
    }
}
