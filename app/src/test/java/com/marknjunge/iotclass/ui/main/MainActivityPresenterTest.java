package com.marknjunge.iotclass.ui.main;

import com.marknjunge.iotclass.MockDatabase;
import com.marknjunge.iotclass.models.Section;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.util.List;

public class MainActivityPresenterTest {
    private Mock mockActivity;

    @Before
    public void setUp() {
        mockActivity = new Mock();
        MockDatabase mockDatabaseRepository = new MockDatabase();
        MainPresenter presenter = new MainPresenter(mockActivity, mockDatabaseRepository);
        presenter.getData();
    }

    @Test
    public void shouldPassSectionsToView() {
        Assert.assertEquals(true, mockActivity.sectionsDisplayed);
    }

    @Test
    public void shouldDisplayNoSectionsAlert() {
        Assert.assertEquals(true, mockActivity.noSectionsAlertDisplayed);
    }

    @Test
    public void shouldDisplayMessageOnScreen() {
        Assert.assertEquals(true, mockActivity.messageDisplayed);
    }

    private class Mock implements MainView {
        private boolean sectionsDisplayed;
        private boolean noSectionsAlertDisplayed;
        private boolean messageDisplayed;

        @Override
        public void displaySections(List<Section> sections) {
            sectionsDisplayed = true;
        }

        @Override
        public void displayNoSectionsAlert() {
            noSectionsAlertDisplayed = true;
        }

        @Override
        public void displayMessage(String message) {
            messageDisplayed = true;
        }
    }

}